FROM clux/muslrust:1.76.0-stable as builder

# Build
COPY . /volume
RUN set -x \
	&& cd /volume \
	&& cargo build --release \
    && strip target/x86_64-unknown-linux-musl/release/hello_world

# Create the root for the minimal image
RUN set -x \
 && mkdir -p /out \
 && cp -r \
    /volume/target/x86_64-unknown-linux-musl/release/hello_world \
	/out/

# Create the minimal image
FROM scratch
COPY --from=builder /out /

ENTRYPOINT ["/hello_world"]

