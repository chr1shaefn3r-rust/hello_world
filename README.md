# Hello World

with added Docker sugar and optimized for small docker image size, because who needs bloated Docker Images anyway, right? Oh, and a Windows and ARM build.

![] (images/docker-image.png)

```bash
$ docker build -t rust-hello-world . && docker run rust-hello-world:latest
```

## Windows build
```bash
$ docker build -t rust-windows -f Dockerfile.windows --network host . && docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/myapp -w /usr/src/myapp rust-windows cargo build --target=x86_64-pc-windows-gnu --release
# optional strip the binary for minimal footprint
$ strip target/x86_64-pc-windows-gnu/release/hello_world.exe
```

## ARM build
```bash
$ docker build -t rust-hello-world-arm -f Dockerfile.arm --network host . && docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/myapp -w /usr/src/myapp rust-hello-world-arm cargo build --target=armv7-unknown-linux-gnueabihf --release
```

## Docker image size over time

 * 1.32: 215kB
 * 1.48: 305kB
 * 1.52: 309kB
 * 1.75: 441kB (but with `<jemalloc>: MADV_DONTNEED does not work (memset will be used instead)`)
 * 1.76: 424kB

## Upgrade editions

from [doc.rust-lang.org](https://doc.rust-lang.org/edition-guide/editions/transitioning-an-existing-project-to-a-new-edition.html)

Rust includes tooling to automatically transition a project from one edition to the next. It will update your source code so that it is compatible with the next edition. Briefly, the steps to update to the next edition are:

 * Run `cargo fix --edition`
 * Edit `Cargo.toml` and set the `edition` field to the next edition, for example `edition = "2021"`
 * Run `cargo build` or `cargo test` to verify the fixes worked.

